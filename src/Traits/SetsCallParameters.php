<?php

namespace Uncgits\EmmaApi\Traits;

trait SetsCallParameters
{
    public function setStart(int $start)
    {
        $this->adapter->setStart($start);
        return $this;
    }

    public function setMaxResults(int $maxResults)
    {
        $this->adapter->setMaxResults($maxResults);
        return $this;
    }

    public function setPerPage(int $perPage)
    {
        $this->config->setPerPage($perPage);
        return $this;
    }

    public function addParameters(array $parameters)
    {
        $this->adapter->addParameters($parameters);
        return $this;
    }

    public function setParameters(array $parameters)
    {
        $this->adapter->setParameters($parameters);
        return $this;
    }

    public function getParameters()
    {
        return $this->adapter->getParameters();
    }

    public function withoutAuthorizationHeader()
    {
        $this->adapter->withoutAuthorizationHeader();
        return $this;
    }

    public function urlEncodeParameters()
    {
        $this->adapter->urlEncodeParameters();
        return $this;
    }
}
