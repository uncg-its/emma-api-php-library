<?php

namespace Uncgits\EmmaApi;

abstract class EmmaApiConfig
{
    /*
    |--------------------------------------------------------------------------
    | Properties
    |--------------------------------------------------------------------------
    |
    | Environment, Authentication, and Configuration information for the client.
    | Set these values in a script or a wrapper for whatever framework you're
    | using.
    |
    */

    /**
     * The host domain for the API, without protocols (default is api.e2ma.net)
     *
     * @var string
     */
    private $apiHost = 'api.e2ma.net/';

    /**
     * The API prefix (default is none for current version of the Emma REST API)
     *
     * @var string
     */
    private $prefix = '';

    /**
     * The API public key to be used in calling the API
     *
     * @var string
     */
    private $publicKey = null;

    /**
     * The API public key to be used in calling the API
     *
     * @var string
     */
    private $privateKey = null;

    /**
     * The account used in calling the API
     *
     * @var string
     */
    private $accountId = null;

    /**
     * The host for the HTTP proxy to be used
     *
     * @var string
     */
    private $proxyHost = null;

    /**
     * The port for the HTTP proxy to be used
     *
     * @var string
     */
    private $proxyPort = null;

    /**
     * Whether to use the HTTP proxy when calling the API
     *
     * @var boolean
     */
    private $useProxy = false;

    /**
     * Fixed limit on the number of results to return from the API. 0 is unlimited.
     *
     * @var integer
     */
    private $maxResults = 0;


    /**
     * Number of results to return per page. Default from Emma is 500.
     *
     * @var integer
     */
    private $perPage = 500;

    /**
     * Destination path to download a file to
     *
     * @var null
     */
    private $downloadPath = null;

    /*
    |--------------------------------------------------------------------------
    | Setters
    |--------------------------------------------------------------------------
    */

    /**
     * @param string $apiHost
     * @return self
     */
    public function setApiHost($apiHost)
    {
        // strip protocol if provided
        $this->apiHost = preg_replace("(^https?://)", "", $apiHost);
        return $this;
    }
    /**
     * @param  string  $prefix
     * @return  self
     */
    public function setPrefix(string $prefix)
    {
        $this->prefix = $prefix;
        return $this;
    }

    /**
     * @param string $publicKey
     * @return self
     */
    public function setPublicKey($publicKey)
    {
        $this->publicKey = $publicKey;
        return $this;
    }

    /**
     * @param string $privateKey
     * @return self
     */
    public function setPrivateKey($privateKey)
    {
        $this->privateKey = $privateKey;
        return $this;
    }

    /**
     * @param string $accountId
     * @return self
     */
    public function setAccountId($accountId)
    {
        $this->accountId = $accountId;
        $this->setPrefix($accountId . '/');
        return $this;
    }

    /**
     * @param mixed $proxyHost
     * @return self
     */
    public function setProxyHost($proxyHost)
    {
        // strip protocol if provided
        $this->proxyHost = preg_replace("(^https?://)", "", $proxyHost);
        return $this;
    }

    /**
     * @param string $proxyPort
     * @return self
     */
    public function setProxyPort(string $proxyPort)
    {
        $this->proxyPort = $proxyPort;
        return $this;
    }

    /**
     * @param boolean $useProxy
     * @return self
     */
    public function setUseProxy(bool $useProxy)
    {
        $this->useProxy = ($useProxy === true);
        return $this;
    }

    /**
     * @param int $maxResults
     * @return self
     */
    public function setMaxResults(int $maxResults)
    {
        $this->maxResults = $maxResults;
        return $this;
    }

    /**
     * Set number of results to return per page. Default from Emma is 500.
     *
     * @param  integer  $perPage  Number of results to return per page. Default from Emma is 500.
     *
     * @return  self
     */
    public function setPerPage($perPage)
    {
        $this->perPage = $perPage;

        return $this;
    }

    /**
     * Set destination path to download a file to
     *
     * @param  string  $downloadPath  Destination path to download a file to
     *
     * @return  self
     */
    public function setDownloadPath($downloadPath)
    {
        $this->downloadPath = $downloadPath;

        return $this;
    }

    /*
    |--------------------------------------------------------------------------
    | Getters
    |--------------------------------------------------------------------------
    */

    /**
     * Get the host domain for the API, without protocols (default is api.e2ma.net)
     *
     * @return  string
     */
    public function getApiHost()
    {
        return $this->apiHost;
    }

    /**
     * Get the API prefix (default is for V1 of the Emma REST API)
     *
     * @return  string
     */
    public function getPrefix()
    {
        return $this->prefix;
    }

    /**
     * Get the API public key to be used in calling the API
     *
     * @return  string
     */
    public function getPublicKey()
    {
        return $this->publicKey;
    }

    /**
     * Get the API private key to be used in calling the API
     *
     * @return  string
     */
    public function getPrivateKey()
    {
        return $this->privateKey;
    }

    /**
     * Get the host for the HTTP proxy to be used
     *
     * @return  string
     */
    public function getProxyHost()
    {
        return $this->proxyHost;
    }

    /**
     * Get whether to use the HTTP proxy when calling the API
     *
     * @return  boolean
     */
    public function getUseProxy()
    {
        return $this->useProxy;
    }

    /**
     * Shortcut to get the formatted proxy string
     *
     * @return  string
     */
    public function getProxy()
    {
        return $this->useProxy ?
            $this->proxyHost . ':' . $this->proxyPort :
            '';
    }

    /**
     * Get fixed limit on the number of results to return from the API. 0 is unlimited.
     *
     * @return  integer
     */
    public function getMaxResults()
    {
        return $this->maxResults;
    }

    /**
     * Get Account ID
     *
     * @return mixed
     */
    public function getAccountId()
    {
        return $this->accountId;
    }

    /**
     * Get number of results to return per page. Default from Emma is 500.
     *
     * @return  integer
     */
    public function getPerPage()
    {
        return $this->perPage;
    }

    /**
     * Get destination path to download a file to
     *
     * @return  null
     */
    public function getDownloadPath()
    {
        return $this->downloadPath;
    }
}
