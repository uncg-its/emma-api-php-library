<?php

namespace Uncgits\EmmaApi;

use Uncgits\EmmaApi\Traits\SetsCallParameters;
use Uncgits\EmmaApi\Clients\EmmaApiClientInterface;
use Uncgits\EmmaApi\Adapters\EmmaApiAdapterInterface;
use Uncgits\EmmaApi\Exceptions\EmmaApiClientException;
use Uncgits\EmmaApi\Exceptions\EmmaApiConfigException;
use Uncgits\EmmaApi\Exceptions\EmmaApiAdapterException;

class EmmaApi
{
    use SetsCallParameters;

    protected $client;
    protected $adapter;
    protected $config;

    protected $tempClient;

    public function __construct(array $setup = [])
    {
        $this->setClient($setup['client'] ?: null);
        $this->setAdapter($setup['adapter'] ?: null);
        $this->setConfig($setup['config'] ?: null);
    }

    public function setClient($client)
    {
        if (is_null($client)) {
            return;
        }

        if (is_string($client) && class_exists($client)) {
            $client = new $client;
        }

        if ($client instanceof EmmaApiClientInterface) {
            $this->client = $client;
            return $this;
        }

        throw new EmmaApiClientException('Unknown or invalid Emma API Client class.');
    }

    public function setAdapter($adapter)
    {
        if (is_null($adapter)) {
            return;
        }


        if (is_string($adapter) && class_exists($adapter)) {
            $adapter = new $adapter;
        }

        if ($adapter instanceof EmmaApiAdapterInterface) {
            $this->adapter = $adapter;
            return $this;
        }

        throw new EmmaApiAdapterException('Unknown or invalid Emma API Adapter.');
    }

    public function setConfig($config)
    {
        if (is_null($config)) {
            return;
        }

        if (is_string($config) && class_exists($config)) {
            $config = new $config;
        }

        if (is_a($config, EmmaApiConfig::class)) {
            $this->config = $config;
            return $this;
        }

        throw new EmmaApiConfigException('Client class must receive EmmaApiConfig object or class name in constructor');
    }

    public function using($client)
    {
        // assume default client location in package unless we were given something that looks namespaced
        if (strpos($client, '\\') === false) {
            $client = '\\Uncgits\\EmmaApi\\Clients\\' . str_replace(' ', '', ucwords($client));
        }

        if (!class_exists($client)) {
            throw new \Exception('Client class ' . $client . ' not found');
        }

        $this->tempClient = new $client;
        return $this;
    }

    public function account($accountId)
    {
        $this->config->setAccountId($accountId);
        return $this;
    }

    public function downloadTo($downloadPath)
    {
        $this->config->setDownloadPath($downloadPath);
        return $this;
    }

    public function __call($method, $arguments)
    {
        // determine active client
        $activeClient = $this->tempClient ?: $this->client;

        // do we have a valid client set?
        if (is_null($activeClient)) {
            throw new EmmaApiClientException('Client is not set on API class');
        }

        // do we know what account we're using?
        if (is_null($this->config->getAccountId())) {
            throw new EmmaApiConfigException('No Account ID is specified on the Config class');
        }

        $this->tempClient = null; // reset

        $endpointParameters = $activeClient->$method(...$arguments);
        $endpoint = (new EmmaApiEndpoint(...$endpointParameters))
        ->setFinalEndpoint($this->config);

        return $this->execute($activeClient, $endpoint, $method, $arguments);
    }

    public function execute(EmmaApiClientInterface $client, EmmaApiEndpoint $endpoint, $method, $arguments)
    {
        return new EmmaApiResult($this->adapter->usingConfig($this->config)->transaction($endpoint));
    }
}
