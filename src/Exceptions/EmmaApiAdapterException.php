<?php

namespace Uncgits\EmmaApi\Exceptions;

class EmmaApiAdapterException extends EmmaApiException
{
}
