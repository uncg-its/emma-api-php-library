<?php

namespace Uncgits\EmmaApi\Exceptions;

class EmmaApiParameterException extends EmmaApiException
{
}
