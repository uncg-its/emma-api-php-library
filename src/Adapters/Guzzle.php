<?php

namespace Uncgits\EmmaApi\Adapters;

use GuzzleHttp\Client;
use Uncgits\EmmaApi\EmmaApiConfig;
use Uncgits\EmmaApi\EmmaApiEndpoint;
use Uncgits\EmmaApi\Traits\ExecutesEmmaApiCalls;

class Guzzle implements EmmaApiAdapterInterface
{
    use ExecutesEmmaApiCalls;

    /*
    |--------------------------------------------------------------------------
    | Implementing EmmaApiAdapterInterface
    |--------------------------------------------------------------------------
    */

    public function call($endpoint, $method)
    {
        // instantiate Guzzle client
        $client = new Client;

        // params / body
        if (count($this->parameters) > 0) {
            if (strtolower($method) === 'get' || $this->urlEncodeParameters) {
                // this is to support include[] and similar...
                $string = http_build_query($this->parameters, null, '&');
                $string = preg_replace('/%5B\d+%5D=/', '%5B%5D=', $string);
                $requestOptions['query'] = $string;

                // GET requests that need pagination will pass params back in the query string of pagination headers.
                $this->setParameters([]);
            } else {
                $requestOptions['json'] = $this->parameters;
            }
        }

        // set proxy settings. set explicitly to empty string if not being used.
        $requestOptions['proxy'] = $this->config->getProxy();

        // authentication
        $requestOptions['auth'] = [$this->config->getPublicKey(), $this->config->getPrivateKey()];

        // additional headers
        if (count($this->additionalHeaders) > 0) {
            $requestOptions['headers'] = array_merge($requestOptions['headers'], $this->additionalHeaders);
        }

        // disable Guzzle exceptions. this class is responsible for providing an account of what happened, so we need
        // to get the response back no matter what.
        $requestOptions['http_errors'] = false;

        // downloads
        if (!is_null($this->config->getDownloadPath())) {
            $requestOptions['sink'] = $this->config->getDownloadPath();
        }

        // perform the call
        $response = $client->$method($endpoint, $requestOptions);

        // normalize the result
        return $this->normalizeResult($endpoint, $method, $requestOptions, $response);
    }

    public function usingConfig(EmmaApiConfig $config)
    {
        $this->config = $config;
        return $this;
    }

    public function transaction(EmmaApiEndpoint $endpoint, $calls = [])
    {
        // set up
        $this->validateParameters($endpoint);

        // if not paginated, one call is sufficient.
        if (!$endpoint->paginated()) {
            $calls[] = $result = $this->call($endpoint->getEndpoint(), $endpoint->getMethod());
            $this->cleanUp();
            return $calls;
        }

        // per documentation, make the initial call to get count. this will help us calculate our pagination.
        $calls[] = $countResult = $this->call($endpoint->getCountEndpoint(), $endpoint->getMethod());
        $totalRecordsAvailable = $countResult['response']['body'];

        // if totalRecordsAvailable is 0, no calls need to be made.
        if ($totalRecordsAvailable === 0) {
            $this->cleanUp();
            return $calls;
        }

        // loop through pagination array and make each call
        $recordsRetrieved = 0;
        foreach ($this->calculatePagination($totalRecordsAvailable) as $page) {
            $this->setParameters([
                'start' => $page['start'],
                'end'   => $page['end'],
            ]);

            $calls[] = $result = $this->call($endpoint->getEndpoint(), $endpoint->getMethod());
            $recordsRetrieved += count($result['response']['body']);
        }

        // clean up
        $this->cleanUp();
        return $calls;
    }

    /**
     * Normalizes and formats API call information into an array for convenience
     *
     * @param string $endpoint
     * @param string $method
     * @param array $requestOptions
     * @param GuzzleHttp\Psr7\Response $response
     * @return void
     */
    public function normalizeResult($endpoint, $method, $requestOptions, $response)
    {
        return [
            'request' => [
                'endpoint'   => $endpoint,
                'query'      => $requestOptions['query'] ?? '',
                'method'     => $method,
                'headers'    => $requestOptions['headers'] ?? [],
                'proxy'      => $this->config->getProxy(),
                'parameters' => $this->parameters,
            ],
            'response' => [
                'headers'              => $response->getHeaders(),
                'code'                 => $response->getStatusCode(),
                'reason'               => $response->getReasonPhrase(),
                'body'                 => json_decode($response->getBody()->getContents())
            ],
        ];
    }

    public function calculatePagination($totalRecordsAvailable)
    {
        $pages = [];
        $perPage = $this->config->getPerPage();

        // calculate how many records to get from either provided value or max available results
        $recordsToGet = $this->maxResults ?: $totalRecordsAvailable - $this->start;
        // if asked for more than are available, default to what's available.
        if ($recordsToGet > $totalRecordsAvailable - $this->start) {
            $recordsToGet = $totalRecordsAvailable - $this->start;
        }

        // adjust perPage if greater than records to get
        if ($perPage > $recordsToGet) {
            $perPage = $recordsToGet;
        }

        $numberOfPages = floor($recordsToGet / $perPage);
        // if it happens to divide evenly then great. if not then we need to add 1
        if ($recordsToGet % $perPage !== 0) {
            $numberOfPages++;
        }

        for ($i = 0; $i < $numberOfPages; $i++) {
            // NOTE FOR FUTURE SELF: $start is inclusive, 0-based. $end is exclusive.
            $start = $this->start + ($i * $perPage);
            $end = $this->start + (($i + 1) * $perPage);
            if ($end - $this->start > $recordsToGet) {
                $end = $this->start + $recordsToGet;
            }
            $pages[] = [
                'start' => $start,
                'end'   => $end
            ];
        }

        return $pages;
    }

    public function cleanUp()
    {
        $this->setParameters([]);
        $this->setRequiredParameters([]);
        $this->urlEncodeParameters = false;
        $this->withAuthorizationHeader = true;
    }
}
