<?php

namespace Uncgits\EmmaApi\Clients;

/**
 * https://api.myemma.com/api/external/signup_forms.html
 */
class SignupForms implements EmmaApiClientInterface
{
    public function listSignupForms()
    {
        return [
            'signup_forms',
            'get',
            [],
            true
        ];
    }
}
