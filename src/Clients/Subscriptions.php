<?php

namespace Uncgits\EmmaApi\Clients;

/**
 * https://api.myemma.com/api/external/subscriptions.html
 */
class Subscriptions implements EmmaApiClientInterface
{
    public function listSubscriptions()
    {
        return [
            'subscriptions',
            'get',
            [],
            true
        ];
    }

    public function getSubscription($subscription_id) // NB: possible typo in Emma docs?
    {
        return [
            'subscriptions/' . $subscription_id,
            'get'
        ];
    }

    public function listMembersForSubscription($subscription_id)
    {
        return [
            'subscriptions/' . $subscription_id . '/members',
            'get',
            [],
            true
        ];
    }

    public function listOptoutsForSubscription($subscription_id)
    {
        return [
            'subscriptions/' . $subscription_id . '/optouts',
            'get',
            [],
            true
        ];
    }

    public function createSubscription()
    {
        return [
            'subscriptions',
            'post',
            ['name', 'description']
        ];
    }

    public function bulkSubscribeMembers($subscription_id)
    {
        return [
            'subscriptions/' . $subscription_id . '/members/bulk',
            'post'
        ];
    }

    public function updateSubscription($subscription_id)
    {
        return [
            'subscriptions/' . $subscription_id,
            'put'
        ];
    }

    public function deleteSubscription($subscription_id)
    {
        return [
            'subscriptions/' . $subscription_id,
            'delete'
        ];
    }
}
