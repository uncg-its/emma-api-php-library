<?php

namespace Uncgits\EmmaApi\Clients;

/**
 * https://api.myemma.com/api/external/response.html
 */
class Response implements EmmaApiClientInterface
{
    public function getResponse()
    {
        return [
            'response',
            'get',
            [],
        ];
    }

    public function getResponseForMailing($mailing_id)
    {
        return [
            'response/' . $mailing_id,
            'get',
            [],
        ];
    }

    public function listSendsForMailing($mailing_id)
    {
        return [
            'response/' . $mailing_id . '/sends',
            'get',
            [],
            true
        ];
    }

    public function listQueuedMessages($mailing_id)
    {
        return [
            'response/' . $mailing_id . '/in_progress',
            'get',
            [],
            true
        ];
    }

    public function listDeliveredMessages($mailing_id)
    {
        return [
            'response/' . $mailing_id . '/deliveries',
            'get',
            [],
            true
        ];
    }

    public function listOpens($mailing_id)
    {
        return [
            'response/' . $mailing_id . '/opens',
            'get',
            [],
            true
        ];
    }

    public function wasMailingOpenedByMember($mailing_id, $member_id)
    {
        return [
            'response/' . $mailing_id . '/opens/' . $member_id,
            'get'
        ];
    }

    // alias
    public function wasOpened($mailing_id, $member_id)
    {
        return $this->wasMailingOpenedByMember($mailing_id, $member_id);
    }

    public function listLinksForMailing($mailing_id)
    {
        return [
            'response/' . $mailing_id . '/links',
            'get',
            [],
            true
        ];
    }

    public function listClicksForMailing($mailing_id)
    {
        return [
            'response/' . $mailing_id . '/clicks',
            'get',
            [],
            true
        ];
    }

    public function listClicksForMember($mailing_id, $member_id)
    {
        return [
            'response/' . $mailing_id . '/clicks/' . $member_id,
            'get',
            [],
            true
        ];
    }

    public function listForwardsForMailing($mailing_id)
    {
        return [
            'response/' . $mailing_id . '/forwards',
            'get',
            [],
            true
        ];
    }

    public function listOptoutsForMailing($mailing_id)
    {
        return [
            'response/' . $mailing_id . '/optouts',
            'get',
            [],
            true
        ];
    }

    public function listSignupsForMailing($mailing_id)
    {
        return [
            'response/' . $mailing_id . '/signups',
            'get',
            [],
            true
        ];
    }

    public function listSharesForMailing($mailing_id)
    {
        return [
            'response/' . $mailing_id . '/shares',
            'get',
            [],
            true
        ];
    }

    public function listCustomerSharesForMailing($mailing_id)
    {
        return [
            'response/' . $mailing_id . '/customer_shares',
            'get',
            [],
            true
        ];
    }

    public function listCustomerShareClicksForMailing($mailing_id)
    {
        return [
            'response/' . $mailing_id . '/customer_share_clicks',
            'get',
            [],
            true
        ];
    }

    public function getCustomerShare($share_id)
    {
        return [
            'response/' . $share_id . '/optouts',
            'get'
        ];
    }

    public function updateCustomerShareStatus($mailing_id)
    {
        return [
            'response/' . $mailing_id . '/customer_share_status',
            'put',
            ['status_to', 'share_id']
        ];
    }

    public function getSharesOverviewForMailing($mailing_id)
    {
        return [
            'response/' . $mailing_id . '/shares/overview',
            'get'
        ];
    }

    public function getSplitTestInfoForMailing($mailing_id)
    {
        return [
            'response/' . $mailing_id . '/split_test_info',
            'get'
        ];
    }
}
