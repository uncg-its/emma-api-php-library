<?php

namespace Uncgits\EmmaApi\Clients;

/**
 * https://api.myemma.com/api/external/groups.html
 */
class Groups implements EmmaApiClientInterface
{
    public function listGroups()
    {
        return [
            'groups',
            'get',
            [],
            true
        ];
    }

    public function createGroups()
    {
        return [
            'groups',
            'post',
            ['groups']
        ];
    }

    public function getMemberGroup($member_group_id)
    {
        return [
            'groups/' . $member_group_id,
            'get'
        ];
    }

    // alias
    public function getGroup($member_group_id)
    {
        return $this->getMemberGroup($member_group_id);
    }

    public function updateMemberGroup($member_group_id)
    {
        return [
            'groups/' . $member_group_id,
            'put'
        ];
    }

    // alias
    public function updateGroup($member_group_id)
    {
        return $this->updateMemberGroup($member_group_id);
    }

    public function deleteMemberGroup($member_group_id)
    {
        return [
            'groups/' . $member_group_id,
            'delete'
        ];
    }

    // alias
    public function deleteGroup($member_group_id)
    {
        return $this->deleteMemberGroup($member_group_id);
    }

    public function listGroupMembers($member_group_id)
    {
        return [
            'groups/' . $member_group_id . '/members',
            'get',
            [],
            true
        ];
    }

    // alias
    public function listMembers($member_group_id)
    {
        return $this->listGroupMembers($member_group_id);
    }

    public function addMembersToGroup($member_group_id)
    {
        return [
            'groups/' . $member_group_id . '/members',
            'put',
            ['member_ids']
        ];
    }

    public function removeGroupMembers($member_group_id)
    {
        return [
            'groups/' . $member_group_id . '/members/remove',
            'put',
            ['member_ids']
        ];
    }

    public function removeAllGroupMembers($member_group_id)
    {
        return [
            'groups/' . $member_group_id . '/members',
            'delete'
        ];
    }

    public function removeAllGroupMembersWithStatus($member_group_id)
    {
        return [
            'groups/' . $member_group_id . '/members/remove',
            'delete',
            ['member_status_id']
        ];
    }

    public function copyGroupMembers($from_group_id, $to_group_id)
    {
        return [
            'groups/' . $from_group_id . '/' . $to_group_id . '/members/copy',
            'put'
        ];
    }
}
