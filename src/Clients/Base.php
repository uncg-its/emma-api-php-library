<?php

namespace Uncgits\EmmaApi\Clients;

class Base implements EmmaApiClientInterface
{
    public function makeCallToRawUrl($url, $type = 'get')
    {
        return [
            $url,
            $type,
            [],
            true
        ];
    }
}
