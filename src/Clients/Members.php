<?php

namespace Uncgits\EmmaApi\Clients;

/**
 * https://api.myemma.com/api/external/members.html
 */
class Members implements EmmaApiClientInterface
{
    public function listMembers()
    {
        return [
            'members',
            'get',
            [],
            true
        ];
    }

    public function getMember($member_id)
    {
        return [
            'members/' . $member_id,
            'get'
        ];
    }

    public function getMemberByEmail($email)
    {
        return [
            'members/email/' . $email,
            'get'
        ];
    }

    public function getOptout($member_id)
    {
        return [
            'members/' . $member_id . '/output',
            'get'
        ];
    }

    public function optOutByEmail($email)
    {
        return [
            'members/email/optout/' . $email,
            'put'
        ];
    }

    public function addMembers()
    {
        return [
            'members',
            'post',
            ['members']
        ];
    }

    public function addOrUpdateMember()
    {
        return [
            'members/add',
            'post',
            ['email', 'fields']
        ];
    }

    // alias
    public function addMember()
    {
        return $this->addOrUpdateMember();
    }

    public function signupMember()
    {
        return [
            'members/signup',
            'post',
            ['email', 'group_ids']
        ];
    }

    public function deleteMembers()
    {
        return [
            'members/delete',
            'put',
            ['member_ids']
        ];
    }

    public function changeMemberStatus()
    {
        return [
            'members/status',
            'put',
            ['member_ids', 'status_to']
        ];
    }

    public function updateMember($member_id)
    {
        return [
            'members/' . $member_id,
            'put'
        ];
    }

    public function deleteMember($member_id)
    {
        return [
            'members/' . $member_id,
            'delete'
        ];
    }

    public function listGroupsForMember($member_id)
    {
        return [
            'members/' . $member_id . '/groups',
            'get',
            [],
            true
        ];
    }

    public function addMemberToGroups($member_id)
    {
        return [
            'members/' . $member_id . '/groups',
            'put',
            ['group_ids']
        ];
    }

    public function removeMemberFromGroups($member_id)
    {
        return [
            'members/' . $member_id . '/groups/remove',
            'put',
            ['group_ids']
        ];
    }

    public function deleteAllMembers()
    {
        return [
            'members',
            'delete'
        ];
    }

    public function removeMemberFromAllGroups($member_id)
    {
        return [
            'members/' . $member_id . '/groups',
            'delete'
        ];
    }

    public function removeMembersFromGroups()
    {
        return [
            'members/groups/remove',
            'put',
            ['member_ids', 'group_ids']
        ];
    }

    public function listMailingHistoryForMember($member_id)
    {
        return [
            'members/' . $member_id . '/mailings',
            'get',
            [],
            true
        ];
    }

    public function listMembersAffectedByImport($import_id)
    {
        return [
            'members/imports/' . $import_id . '/members',
            'get',
            [],
            true
        ];
    }

    public function getImport($import_id)
    {
        return [
            'members/imports/' . $import_id,
            'get'
        ];
    }

    public function listImports()
    {
        return [
            'members/imports',
            'get',
            [],
            true
        ];
    }

    public function copyMembersIntoGroup($group_id)
    {
        return [
            'members/' . $group_id . '/copy',
            'put',
            ['member_status_id']
        ];
    }

    public function updateMemberStatus($status_from, $status_to)
    {
        return [
            'members/status/' . $status_from . '/to/' . $status_to,
            'put'
        ];
    }

    public function purgeMembers()
    {
        return [
            'members/purge',
            'post',
            ['member_ids']
        ];
    }

    public function purgeDeletedMembers()
    {
        return [
            'members/purgeall',
            'put'
        ];
    }
}
