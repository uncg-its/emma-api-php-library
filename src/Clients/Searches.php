<?php

namespace Uncgits\EmmaApi\Clients;

/**
 * https://api.myemma.com/api/external/searches.html
 */
class Searches implements EmmaApiClientInterface
{
    public function listSearches()
    {
        return [
            'searches',
            'get',
            [],
            true
        ];
    }

    public function getSearch($search_id)
    {
        return [
            'searches/' . $search_id,
            'get'
        ];
    }

    public function createSearch()
    {
        return [
            'searches',
            'post',
            ['criteria', 'name']
        ];
    }

    public function updateSearch($search_id)
    {
        return [
            'searches/' . $search_id,
            'put'
        ];
    }

    public function deleteSearch($search_id)
    {
        return [
            'searches/' . $search_id,
            'delete'
        ];
    }

    public function listMembersForSearch($search_id)
    {
        return [
            'searches/' . $search_id . '/members',
            'get',
            [],
            true
        ];
    }
}
