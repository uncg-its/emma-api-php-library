<?php

namespace Uncgits\EmmaApi\Clients;

/**
 * https://api.myemma.com/api/external/mailings.html
 */
class Mailings implements EmmaApiClientInterface
{
    public function listMailings()
    {
        return [
            'mailings',
            'get',
            [],
            true
        ];
    }

    public function getCalendar()
    {
        return [
            'calendar',
            'get'
        ];
    }

    public function getMailing($mailing_id)
    {
        return [
            'mailings/' . $mailing_id,
            'get'
        ];
    }

    public function listMembersForMailing($mailing_id)
    {
        return [
            'mailings/' . $mailing_id . '/members',
            'get',
            [],
            true
        ];
    }

    public function getMailingContentSentToMember($mailing_id, $member_id)
    {
        return [
            'mailings/' . $mailing_id . '/messages/' . $member_id,
            'get'
        ];
    }

    //alias
    public function getMailingContent($mailing_id, $member_id)
    {
        return $this->getMailingContentSentToMember($mailing_id, $member_id);
    }

    public function listGroupsForMailing($mailing_id)
    {
        return [
            'mailings/' . $mailing_id . '/groups',
            'get',
            [],
            true
        ];
    }

    public function listSubscriptionsForMailing($mailing_id)
    {
        return [
            'mailings/' . $mailing_id . '/subscriptions',
            'get',
            [],
            true
        ];
    }

    public function listSearchesForMailing($mailing_id)
    {
        return [
            'mailings/' . $mailing_id . '/searches',
            'get',
            [],
            true
        ];
    }

    public function listSuppressedSearchesForMailing($mailing_id)
    {
        return [
            'mailings/' . $mailing_id . '/suppressed_searches',
            'get',
            [],
            true
        ];
    }

    public function listSuppressedMembersForMailing($mailing_id)
    {
        return [
            'mailings/' . $mailing_id . '/suppressed_members',
            'get',
            [],
            true
        ];
    }

    public function updateMailingStatus($mailing_id)
    {
        return [
            'mailings/' . $mailing_id,
            'put',
            ['status']
        ];
    }

    public function archiveMailing($mailing_id)
    {
        return [
            'mailings/' . $mailing_id,
            'delete'
        ];
    }

    public function cancelMailing($mailing_id)
    {
        return [
            'mailings/cancel/' . $mailing_id,
            'delete'
        ];
    }

    public function forwardMailingSentToMember($mailing_id, $member_id)
    {
        return [
            'forwards/' . $mailing_id . '/' . $member_id,
            'post'
        ];
    }

    // alias
    public function forwardMailing($mailing_id, $member_id)
    {
        return $this->forwardMailingSentToMember($mailing_id, $member_id);
    }

    public function resendMailing($mailing_id)
    {
        return [
            'mailings/' . $mailing_id,
            'post'
        ];
    }

    public function listHeadsUpAddressesForMailing($mailing_id)
    {
        return [
            'mailings/' . $mailing_id . '/headsup',
            'get',
            [],
            true
        ];
    }

    public function validateTagSyntax()
    {
        return [
            'mailings/validate',
            'post'
        ];
    }

    public function declareWinnerForMailing($mailing_id, $winner_id)
    {
        return [
            'mailings/' . $mailing_id . '/winner/' . $winner_id,
            'post'
        ];
    }
}
