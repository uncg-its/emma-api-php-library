<?php

namespace Uncgits\EmmaApi\Clients;

/**
 * https://api.myemma.com/api/external/fields.html
 */
class Fields implements EmmaApiClientInterface
{
    /*
    |--------------------------------------------------------------------------
    | Methods
    |--------------------------------------------------------------------------
    */

    public function listFields()
    {
        return [
            'fields',
            'get',
            [],
            true
        ];
    }

    public function getField($field_id)
    {
        return [
            'fields/' . $field_id,
            'get'
        ];
    }

    public function createField()
    {
        return [
            'fields',
            'post'
        ];
    }

    public function deleteField($field_id)
    {
        return [
            'fields/' . $field_id,
            'delete'
        ];
    }

    public function clearField($field_id)
    {
        return [
            'fields/' . $field_id . '/clear',
            'post'
        ];
    }

    public function updateField($field_id)
    {
        return [
            'fields/' . $field_id,
            'put'
        ];
    }
}
