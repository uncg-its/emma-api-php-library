<?php

namespace Uncgits\EmmaApi\Clients;

/**
 * https://api.myemma.com/api/external/exports.html

 */
class Exports implements EmmaApiClientInterface
{
    public function listExports()
    {
        return [
            'exports',
            'get',
            [],
            true
        ];
    }

    public function getExport($export_id)
    {
        return [
            'exports/' . $export_id,
            'get'
        ];
    }

    public function downloadExport($export_id)
    {
        return [
            'exports/' . $export_id . '/download',
            'get'
        ];
    }

    public function createExport()
    {
        return [
            'exports',
            'post',
            ['user_id', 'criteria']
        ];
    }

    public function createExportForMember($member_id)
    {
        return [
            'exports/members/' . $member_id,
            'post'
        ];
    }
}
