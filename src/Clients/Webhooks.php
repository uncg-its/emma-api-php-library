<?php

namespace Uncgits\EmmaApi\Clients;

/**
 * https://api.myemma.com/api/external/webhooks.html
 */
class Webhooks implements EmmaApiClientInterface
{
    public function listWebhooks()
    {
        return [
            'webhooks',
            'get',
            [],
            true
        ];
    }

    public function getWebhook($webhook_id)
    {
        return [
            'webhooks/' . $webhook_id,
            'get'
        ];
    }

    public function listAvailableEvents()
    {
        return [
            'webhooks/events',
            'get',
            [],
            true
        ];
    }

    public function createWebhook()
    {
        return [
            'webhooks',
            'post',
            ['event', 'url', 'public_key']
        ];
    }

    public function updateWebhook($webhook_id)
    {
        return [
            'webhooks/' . $webhook_id,
            'put'
        ];
    }

    public function deleteWebhook($webhook_id)
    {
        return [
            'webhooks/' . $webhook_id,
            'delete'
        ];
    }

    public function deleteAllWebhooks()
    {
        return [
            'webhooks',
            'delete'
        ];
    }
}
