<?php

namespace Uncgits\EmmaApi\Clients;

/**
 * https://api.myemma.com/api/external/automation.html
 */
class Automation implements EmmaApiClientInterface
{
    public function listWorkflows()
    {
        return [
            'automation/workflows',
            'get',
            [],
            true
        ];
    }

    public function getWorkflow($workflow_id)
    {
        return [
            'automation/workflows/' . $workflow_id,
            'get'
        ];
    }

    public function getWorkflowCountByState()
    {
        return [
            'automation/counts',
            'get'
        ];
    }

    // alias
    public function getWorkflowCount()
    {
        return $this->getWorkflowCountByState();
    }
}
